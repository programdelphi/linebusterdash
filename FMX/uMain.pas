unit uMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,system.strUtils,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.ADS, FireDAC.Phys.ADSDef, FireDAC.FMXUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FMX.Controls.Presentation, FMX.StdCtrls, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Data.Bind.EngExt, Fmx.Bind.DBEngExt, FMX.TMSLiveGridDataBinding, System.Rtti,
  System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,
  FMX.TMSBaseControl, FMX.TMSGridCell, FMX.TMSGridOptions, FMX.TMSGridData, FMX.TMSCustomGrid, FMX.TMSLiveGrid,
  FMX.TMSGridFilterPanel, FMX.Grid.Style, Fmx.Bind.Grid, FMX.ScrollBox, FMX.Grid, FMX.Edit, FMX.EditBox, FMX.SpinBox;

type
  TForm3 = class(TForm)
    FDConnection1: TFDConnection;
    FDQuery1: TFDQuery;
    StringGrid1: TStringGrid;
    BindingsList1: TBindingsList;
    LinkGridToDataSource1: TLinkGridToDataSource;
    BindSourceDB1: TBindSourceDB;
    tmr1: TTimer;
    pnlboutom: TPanel;
    btnOpen: TButton;
    btnRefresh: TButton;
    lblDays: TLabel;
    SpinBox1: TSpinBox;
    lblVer: TLabel;
    pnlSums: TPanel;
    FDQuery2: TFDQuery;
    lblPushToPos: TLabel;
    FDQuery3: TFDQuery;
    lblshopping: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure SpinBox1Change(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
  private
    procedure OpenDB;
    procedure DataRefresh;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.fmx}

procedure TForm3.FormCreate(Sender: TObject);
begin
OpenDB ;
tmr1.Enabled := True ;
end;

procedure TForm3.btnOpenClick(Sender: TObject);
 begin
  OpenDB;
 end;

procedure TForm3.btnRefreshClick(Sender: TObject);
begin
  DataRefresh;
end;

procedure TForm3.tmr1Timer(Sender: TObject);
begin
DataRefresh  ;
end;

procedure TForm3.OpenDB;
begin
  //vSql := 'Select s.* ,TimeStampDiff(SQL_TSI_MINUTE, s.StartTime, Now()) StartToNow, TimeStampDiff(SQL_TSI_MINUTE, s.StartTime, s.EndTime) StartToEnd    From SelfCheckoutStatus s Where s.StartTime>=Cast(Date()-%f AS SQL_TIMESTAMP)';
  //FDQuery1.SQL.Text := Format(vSql, [SpinBox1.Value]);
  FDQuery1.Close;
  FDQuery1.ParamByName('DaysBack').AsInteger := Round(SpinBox1.Value);
  FDQuery1.Open;
  FDQuery2.Close;
  FDQuery2.ParamByName('DaysBack').AsInteger := Round(SpinBox1.Value);
  FDQuery2.Open;
  FDQuery3.Close;
  FDQuery3.ParamByName('DaysBack').AsInteger := Round(SpinBox1.Value);
  FDQuery3.Open;
end;

procedure TForm3.DataRefresh;
begin
  FDQuery1.DisableControls;
  try
    FDQuery1.Close;
    FDQuery1.Open;
  finally
    FDQuery1.EnableControls;
  end;
  FDQuery2.Close;
  FDQuery2.Open;
  lblPushToPos.Text := Format('Push To POS: %d', [FDQuery2.Fields[0].AsInteger]);
  FDQuery3.Close;
  FDQuery3.Open;
  lblshopping.Text := Format('Active Shoppers: %d', [FDQuery3.Fields[0].AsInteger]);
end;

procedure TForm3.SpinBox1Change(Sender: TObject);
begin
OpenDB ;
end;

end.
